package my.audioplayer.app.utils;

import java.util.concurrent.TimeUnit;

public final class TimeUtils {
    private static TimeUtils instance;

    private TimeUtils() {
    }

    public static synchronized TimeUtils getInstance() {
        if (instance == null) instance = new TimeUtils();

        return instance;
    }

//    mms formatter -> (MSSFormat)
    public String formatter(String millis) {
        int value = 0;
        try {
            value = Integer.valueOf(millis);
        } catch (Exception e) {
            e.printStackTrace();
        }

        long seconds = TimeUnit.MILLISECONDS.toSeconds(value);
        long minutes = seconds / 60;
        seconds = seconds - minutes * 60;

        return String.format("%01d:%02d", minutes, seconds);
    }
}
