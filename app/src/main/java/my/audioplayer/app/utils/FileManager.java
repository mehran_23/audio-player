package my.audioplayer.app.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

import my.audioplayer.app.App;
import my.audioplayer.app.R;
import my.audioplayer.app.models.SongInfo;

/**
 * use singleton pattern for stateLess class
 */
public final class FileManager {
    private static FileManager instance;
    private TimeUtils timeUtils;

    private FileManager(TimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    public static synchronized FileManager getInstance() {
        if (instance == null) instance = new FileManager(TimeUtils.getInstance());

        return instance;
    }

    public List<SongInfo> loadMusicFiles(ContentResolver contentResolver) {
        List<SongInfo> songInfoList = new ArrayList<>();

        //        query options
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!=0";
        String[] projection = {
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.DURATION,
        };

        //        use content provider for fetch music file
        Cursor cursor = contentResolver.query(uri,
                projection,
                selection,
                null,
                MediaStore.Audio.Media.DATA + " COLLATE NOCASE ASC",
                null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
                    String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String id = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

                    Uri sArtworkUri = Uri.parse(App.getInstance().getString(R.string.file_manager_album_uri));
                    Uri cover = ContentUris.withAppendedId(sArtworkUri, Long.parseLong(id));

                    String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));

                    SongInfo songInfo = new SongInfo(name, artist, url, cover, timeUtils.formatter(duration), Integer.parseInt(duration), id);
                    songInfoList.add(songInfo);

                } while (cursor.moveToNext());

            }

            cursor.close();
        }

        return songInfoList;
    }

}
