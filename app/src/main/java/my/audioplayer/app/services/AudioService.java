package my.audioplayer.app.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.io.IOException;

import my.audioplayer.app.R;
import my.audioplayer.app.recievers.SoundControllerIntentReceiver;
import my.audioplayer.app.ui.MainActivity;
import my.audioplayer.app.ui.dialog.QuickToast;

import static my.audioplayer.app.phrase.Constants.ACTION_PAUSE;
import static my.audioplayer.app.phrase.Constants.ACTION_PLAY;
import static my.audioplayer.app.phrase.Constants.ACTION_RESUME;
import static my.audioplayer.app.phrase.Constants.PLAYER_INITIALIZED_PAUSED;
import static my.audioplayer.app.phrase.Constants.PLAYER_IS_NULL;
import static my.audioplayer.app.phrase.Constants.PLAYER_IS_PLAYING;
import static my.audioplayer.app.phrase.Constants.PLAYER_PAUSED_BY_AFCHANGE;
import static my.audioplayer.app.phrase.Constants.PLAYER_QUIET_CAN_DUCK;

public class AudioService extends Service implements MediaPlayer.OnCompletionListener
        , SoundControllerIntentReceiver.SoundControllerAction {

    private static final int PROGRESS_UPDATE_PERIOD = 100;
    private static final int MSG_UPDATE_PROGRESS = 1;
    public int playerState = PLAYER_IS_NULL;
    public static final String ACTION_FROM_NOTIF_PREV = "ACTION_FROM_NOTIF_PREV";
    public static final String ACTION_FROM_NOTIF_PAUSE = "ACTION_FROM_NOTIF_PAUSE";
    private static final int NOTIFY_PLAYING = 1;
    public static final String ACTION_FROM_NOTIF_PLAY = "ACTION_FROM_NOTIF_PLAY";
    public static final String ACTION_FROM_NOTIF_NEXT = "ACTION_FROM_NOTIF_NEXT";

    private MediaPlayer mediaPlayer = new MediaPlayer();
    private MediaPlayer.OnErrorListener onErrorListener;
    private MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener;

    private OnAudioProgressUpdateListener onAudioProgressUpdateListener;
    private int duration;
    private boolean isComplete;
    private SoundControllerIntentReceiver musicNoisyIntentReceiver;

    private Handler handler;

    //    handle all state of change player focus
    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_GAIN:
                    if (mediaPlayer != null) {
                        if (playerState == PLAYER_PAUSED_BY_AFCHANGE) {
                            playAudioFile();
                            mediaPlayer.setVolume(1.0f, 1.0f);
                        } else if (playerState == PLAYER_QUIET_CAN_DUCK) {
                            mediaPlayer.setVolume(1.0f, 1.0f);
                            playerState = PLAYER_IS_PLAYING;
                        }
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    if (mediaPlayer.isPlaying()) {
                        pausePlayer();
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    if (mediaPlayer.isPlaying()) {
                        makeAFChangedPause();
                    }
                    break;

                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.setVolume(0.1f, 0.1f);
                        playerState = PLAYER_QUIET_CAN_DUCK;
                    }
                    break;
            }
        }
    };

    public void playAudioFile() {
        if (mediaPlayer != null) {
            if (mediaPlayer.getCurrentPosition() != 0) {
                unregisterAudioNoisyFilter();//to ensure that if was previously unregistred
                registerAudioNoisyFilter();
                mediaPlayer.start();
                playerState = 1;

            } else {
                //to ensure that it was previously unregistred
                unregisterAudioNoisyFilter();
                registerAudioNoisyFilter();
                mediaPlayer.start();
                playerState = PLAYER_IS_PLAYING;
            }
        }
    }

    public void pausePlayer() {
        if (musicNoisyIntentReceiver != null) {
            musicNoisyIntentReceiver.setSoundControllerAction(null);
            try {
                unregisterReceiver(musicNoisyIntentReceiver);
            }catch (Exception ignored){

            }
            musicNoisyIntentReceiver = null;
        }
        mediaPlayer.pause();
        playerState = PLAYER_INITIALIZED_PAUSED;
    }

    public void makeAFChangedPause() {

        //pause when audiofocus has been changed
        unregisterAudioNoisyFilter();
        mediaPlayer.pause();
        playerState = PLAYER_PAUSED_BY_AFCHANGE;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new AudioBinder();
    }

    public void changeTrack(String url) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.reset();

            try {
                mediaPlayer.setDataSource(url);
                mediaPlayer.prepare();
                mediaPlayer.start();
                duration = mediaPlayer.getDuration();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("HandlerLeak")
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (onAudioProgressUpdateListener != null) {
                    onAudioProgressUpdateListener.update(duration, mediaPlayer.getCurrentPosition());
                }
                if (!isComplete) {
                    handler.sendEmptyMessageDelayed(MSG_UPDATE_PROGRESS, PROGRESS_UPDATE_PERIOD);
                }
            }
        };

        final String url = intent.getStringExtra("url");

//        this process have high usage and disturb main thread, so we run it in new thread to prevent probable issue
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (ACTION_PLAY.equals(intent.getAction())) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    try {
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        mediaPlayer.setOnCompletionListener(AudioService.this);
                        mediaPlayer.setOnErrorListener(onErrorListener);
                        mediaPlayer.setOnBufferingUpdateListener(onBufferingUpdateListener);
                        duration = mediaPlayer.getDuration();
                        handler.sendEmptyMessage(MSG_UPDATE_PROGRESS);

                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.requestAudioFocus(afChangeListener,
                                // Use the music stream.
                                AudioManager.STREAM_MUSIC,
                                // Request permanent focus.
                                AudioManager.AUDIOFOCUS_GAIN);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (ACTION_PAUSE.equals(intent.getAction())) {
                    mediaPlayer.pause();
                } else if (ACTION_RESUME.equals(intent.getAction())) {
                    mediaPlayer.start();
                }
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        isComplete = true;
        stopSelf();
    }

    //    buffering and streaming callback
    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener) {
        this.onErrorListener = onErrorListener;
    }

    public void setOnBufferingUpdateListener(MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener) {
        this.onBufferingUpdateListener = onBufferingUpdateListener;
    }

    public void setOnAudioProgressUpdateListener(OnAudioProgressUpdateListener onAudioProgressUpdateListener) {
        this.onAudioProgressUpdateListener = onAudioProgressUpdateListener;
    }

    public void setListener(Object listener) {
        setOnErrorListener((MediaPlayer.OnErrorListener) listener);
        setOnBufferingUpdateListener((MediaPlayer.OnBufferingUpdateListener) listener);
        setOnAudioProgressUpdateListener((OnAudioProgressUpdateListener) listener);
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public int getCurrentPosition() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying() || (playerState >= 0)) {
                return mediaPlayer.getCurrentPosition();
            }
        }
        return 0;
    }

    public int getDuration() {
        int duration = 0;
        try {
            duration = mediaPlayer.getDuration();
        } catch (Exception ignored) {
        }
        return duration;
    }

    public class AudioBinder extends Binder {
        public AudioService getService() {
            return AudioService.this;
        }
    }

    public interface OnAudioProgressUpdateListener {
        void update(int duration, int currentPosition);
    }

    //  it's registered as receiver
    private void registerAudioNoisyFilter() {
        IntentFilter audioNoisyFilter;
        audioNoisyFilter = new IntentFilter(android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        musicNoisyIntentReceiver = new SoundControllerIntentReceiver();
        registerReceiver(musicNoisyIntentReceiver, audioNoisyFilter);
        musicNoisyIntentReceiver.setSoundControllerAction(this);
    }

    private void unregisterAudioNoisyFilter() {
        if (musicNoisyIntentReceiver != null) {
            musicNoisyIntentReceiver.setSoundControllerAction(null);
            unregisterReceiver(musicNoisyIntentReceiver);
            musicNoisyIntentReceiver = null;
        }
    }

    @Override
    public void onReceive(Context ctx, Intent intent) {

        // when any kind of sound play, this function call
        if (android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
            if ((mediaPlayer != null) & (mediaPlayer.isPlaying())) {
                pausePlayer();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void foregroundNotification(String textStatus, String textFilename) {

        PendingIntent pIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationChannel chan = new NotificationChannel("1234", "channelName", NotificationManager.IMPORTANCE_DEFAULT);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"123")
                .setSmallIcon(R.drawable.ic_start_simpleplayer)
                // Large icon won't be available on API<11 devices
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(false)
                .setOngoing(true)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(textStatus + textFilename)
                .setTicker(textStatus + textFilename)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent);

        // TODO Set RemoteViews into Notification and image

        Intent movePrev = new Intent(ACTION_FROM_NOTIF_PREV);
        PendingIntent pendingPrevIntent = PendingIntent.getBroadcast(this, 0, movePrev, 0);
        builder.addAction(R.drawable.ic_media_previous, "", pendingPrevIntent);

        if (playerState > 0) {
            Intent pause = new Intent(ACTION_FROM_NOTIF_PAUSE);
            PendingIntent pendingPauseIntent = PendingIntent.getBroadcast(this, 0, pause, 0);
            builder.addAction(R.drawable.ic_media_pause, "", pendingPauseIntent);
        } else {
            Intent play = new Intent(ACTION_FROM_NOTIF_PLAY);
            PendingIntent pendingPlayIntent = PendingIntent.getBroadcast(this, 0, play, 0);
            builder.addAction(R.drawable.ic_media_play, "", pendingPlayIntent);
        }
        Intent moveNext = new Intent(ACTION_FROM_NOTIF_NEXT);
        PendingIntent pendingNextIntent = PendingIntent.getBroadcast(this, 0, moveNext, 0);
        builder.addAction(R.drawable.ic_media_next, "", pendingNextIntent);

        startForeground(NOTIFY_PLAYING, builder.build());
    }

}
