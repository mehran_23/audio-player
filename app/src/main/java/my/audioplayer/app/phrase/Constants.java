package my.audioplayer.app.phrase;

import android.os.Environment;

public class Constants {
//    permissions
    public static final int PERMISSION_ACCESS_STORAGE = 1001;

//    audio service
    public static final String ACTION_PLAY = "com.mzule.audiodemo.PLAY";
    public static final String ACTION_PAUSE = "com.mzule.audiodemo.PAUSE";
    public static final String ACTION_RESUME = "com.mzule.audiodemo.RESUME";

    public static final int PLAYER_IS_NULL = -2;
    public static final int PLAYER_INITIALIZED_PAUSED = 0;
    public static final int PLAYER_IS_PLAYING = 1;
    public static final int PLAYER_PAUSED_BY_AFCHANGE = 2;
    public static final int PLAYER_QUIET_CAN_DUCK = 3;

//    file manager
    public static final String externalStoragePath =
            Environment.getExternalStorageDirectory().getAbsolutePath();

}
