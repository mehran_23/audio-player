package my.audioplayer.app.models;

import android.net.Uri;

public final class SongInfo {
    private String name;
    private String artist;
    private String url;
    private String duration;
    private String cover;
    private int actualDuration;
    private String id;

    public SongInfo(String name, String artist, String url, Uri cover, String duration, int actualDuration, String id) {
        this.name = name;
        this.artist = artist;
        this.url = url;
        this.cover = cover.toString();
        this.duration = duration;
        this.actualDuration = actualDuration;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getActualDuration() {
        return actualDuration;
    }

    public void setActualDuration(int actualDuration) {
        this.actualDuration = actualDuration;
    }

    public Uri getCover() {
        return Uri.parse(cover);
    }

    public void setCover(Uri cover) {
        this.cover = cover.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
