package my.audioplayer.app.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * prevent from incoming sound and noise
 */
public class SoundControllerIntentReceiver extends BroadcastReceiver {
    private SoundControllerAction soundControllerAction;
    @Override
    public void onReceive(Context ctx, Intent intent) {
       if (soundControllerAction!=null)
           soundControllerAction.onReceive(ctx,intent);
    }

    public interface SoundControllerAction{
        void onReceive(Context ctx, Intent intent);
    }

    public void setSoundControllerAction(SoundControllerAction soundControllerAction) {
        this.soundControllerAction = soundControllerAction;
    }
}

