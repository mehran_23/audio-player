package my.audioplayer.app.ui.dialog;

import android.widget.Toast;

import my.audioplayer.app.App;


public class QuickToast {
    public QuickToast(String message) {
        Toast.makeText(App.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    public QuickToast(int idMessage) {
        Toast.makeText(App.getInstance(), App.getInstance().getString(idMessage), Toast.LENGTH_SHORT).show();
    }

}