package my.audioplayer.app.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.google.gson.Gson;

import java.util.List;
import java.util.Random;

import my.audioplayer.app.R;
import my.audioplayer.app.databinding.ActivityMainBinding;
import my.audioplayer.app.models.SongInfo;
import my.audioplayer.app.phrase.Constants;
import my.audioplayer.app.services.AudioService;
import my.audioplayer.app.utils.FileManager;
import my.audioplayer.app.utils.TimeUtils;

import static my.audioplayer.app.phrase.Constants.PERMISSION_ACCESS_STORAGE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
        , MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnErrorListener, AudioService.OnAudioProgressUpdateListener {

    private FileManager fileManager;
    private ActivityMainBinding binding;
    private List<SongInfo> listData;
    private AudioService audioService;
    private final Handler mHandler = new Handler();
    private SongInfo songInfo;
    private TimeUtils timeUtils;
    private final String CURRENT_TRACK = "current_track";
    private static boolean isAppClose = false;
    //    this binder use for access media player
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioService.AudioBinder audioBinder = (AudioService.AudioBinder) service;
            audioService = audioBinder.getService();
            audioService.setListener(MainActivity.this);

            if (audioService != null && audioService.getMediaPlayer().isPlaying()) {
                binding.buttonPlaystop.setImageResource(R.drawable.ic_pause);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        fileManager = FileManager.getInstance();
        timeUtils = TimeUtils.getInstance();
        initViewComponents();

        loadMp3Files();

        showHelpCase();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (songInfo != null)
            outState.putString(CURRENT_TRACK, new Gson().toJson(songInfo));

    }

    // for save activity state , we restore data
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isAppClose = false;
        if (savedInstanceState.containsKey(CURRENT_TRACK)) {
            songInfo = new Gson().fromJson(savedInstanceState.getString(CURRENT_TRACK), SongInfo.class);

            binding.textTrackArtist.setText(songInfo.getArtist());
            binding.textTrackName.setText(songInfo.getName());

            binding.seekbarMain.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (audioService == null) seekBar.setProgress(0);

                    float v = songInfo.getActualDuration();
                    float v1 = (float) seekBar.getProgress() / 100f;
                    float seekTo = v * v1;
                    audioService.getMediaPlayer().seekTo((int) seekTo);
                }
            });
            updateTrackTime();

            Glide.with(this)
                    .asBitmap()

                    .load(songInfo.getCover())
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            Glide.with(MainActivity.this)
                                    .load(resource)
                                    .into(binding.imgCover);
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            Glide.with(MainActivity.this)
                                    .load(R.drawable.music_place_holder)
                                    .into(binding.imgCover);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });

            Intent intent = new Intent(getApplicationContext(), AudioService.class);
            intent.setAction(Constants.ACTION_PLAY);
            intent.putExtra("url", songInfo.getUrl());
            bindService(intent, serviceConnection, 0);
        }
    }


    private void showHelpCase() {
        new BubbleShowCaseBuilder(this)
                .title(getString(R.string.activity_main_shuffle_button))
                .description(getString(R.string.activity_main_shuffle_description))
                .backgroundColor(getResources().getColor(R.color.colorPrimary))
                .textColor(Color.WHITE)
                .targetView(binding.buttonShuffle)
                .showOnce("1001")
                .show();
    }

    private void loadMp3Files() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_ACCESS_STORAGE);
        } else {
            listData = fileManager.loadMusicFiles(getContentResolver());
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViewComponents() {
        binding.buttonPlaystop.setOnClickListener(this);
        binding.buttonShuffle.setOnClickListener(this);

        Glide.with(MainActivity.this)
                .load(R.drawable.music_place_holder)
                .into(binding.imgCover);
    }

    private void shuffle() {
        songInfo = listData.get(new Random().nextInt(listData.size()));

        binding.textTrackArtist.setText(songInfo.getArtist());
        binding.textTrackName.setText(songInfo.getName());


        Glide.with(this)
                .asBitmap()
                .load(songInfo.getCover())
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        Glide.with(MainActivity.this)
                                .load(resource)
                                .into(binding.imgCover);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        Glide.with(MainActivity.this)
                                .load(R.drawable.music_place_holder)
                                .into(binding.imgCover);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });


        binding.seekbarMain.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (audioService == null) seekBar.setProgress(0);

                float v = songInfo.getActualDuration();
                float v1 = (float) seekBar.getProgress() / 100f;
                float seekTo = v * v1;
                audioService.getMediaPlayer().seekTo((int) seekTo);
            }
        });

        if (audioService == null) {
            Intent intent = new Intent(getApplicationContext(), AudioService.class);
            intent.setAction(Constants.ACTION_PLAY);
            intent.putExtra("url", songInfo.getUrl());
            bindService(intent, serviceConnection, 0);
            startService(intent);

            startPlayProgressUpdater();
        } else {

            audioService.changeTrack(songInfo.getUrl());
            startPlayProgressUpdater();
        }

        binding.buttonPlaystop.setImageResource(R.drawable.ic_pause);
    }

    private void buttonPlayStopClick() {
        if (audioService == null) return;
        if (songInfo == null) return;

        if (audioService.getMediaPlayer().isPlaying()) {
            binding.buttonPlaystop.setImageResource(R.drawable.ic_play_button);
            audioService.pausePlayer();
        } else {
            binding.buttonPlaystop.setImageResource(R.drawable.ic_pause);
            audioService.playAudioFile();
        }

    }

    private void updateTrackTime() {
        if (audioService == null) return;

        String leftTimeMMSS = "-"
                + timeUtils.formatter(String.valueOf(audioService.getDuration() - audioService.getCurrentPosition()));
        String currentTimeMMSS = timeUtils
                .formatter(String.valueOf(audioService.getCurrentPosition()));
        binding.textCurrentTracktime.setText(currentTimeMMSS);
        binding.textLeftTracktime.setText(leftTimeMMSS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //request permission for access storage

        if (permissions.length > 0 && requestCode == PERMISSION_ACCESS_STORAGE) {
            if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadMp3Files();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_ACCESS_STORAGE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_playstop:
                buttonPlayStopClick();
                break;

            case R.id.button_shuffle:
                shuffle();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        isAppClose = true;
        try {
            if (audioService != null && audioService.getMediaPlayer().isPlaying()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAppClose) {
                            audioService.getMediaPlayer().stop();
                            audioService.getMediaPlayer().reset();
                        }
                    }
                }, 500);
            }
            unbindService(serviceConnection);

        } catch (Exception ignored) {

        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (audioService != null) {
            audioService.setListener(this);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        startPlayProgressUpdater();
    }

    @Override
    protected void onStop() {
        if (audioService != null) {
            audioService.setListener(null);
        }
        mHandler.removeCallbacksAndMessages(null);
        super.onStop();

    }

    @Override
    public void update(int duration, int currentPosition) {
        binding.seekbarMain.setProgress((int) (currentPosition * 100f / duration));
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        binding.seekbarMain.setSecondaryProgress(percent);

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    public void startPlayProgressUpdater() {
        updateTrackTime();
        Runnable notification = new Runnable() {
            public void run() {
                startPlayProgressUpdater();
            }
        };
        mHandler.postDelayed(notification, 500);

    }
}